/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import com.sun.management.OperatingSystemMXBean;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.management.ManagementFactory;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.CpuTimer;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Namchin Erdene
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    private static Sigar sigar = new Sigar();

    public static void getInformationsAboutMemory() throws ProtocolException, MalformedURLException, IOException   {
        
        
            String url = "";
            URL object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestProperty("Content-type", "application/json; charset=UTF-8");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");
        
        
        Mem mem = null;
        OperatingSystemMXBean bean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        
        try {
            mem = sigar.getMem();
        } catch (SigarException se) {
            se.printStackTrace();
        }
        
        JSONObject jSONObject = new JSONObject();
        JSONArray array = new JSONArray();
        
        HashMap hashMap = new HashMap();
        HashMap cpuinfo = new HashMap();
        
        try {
            CpuInfo[] info;
            info = sigar.getCpuInfoList();
            
            long getSystemCpuLoad = (long) bean.getSystemCpuLoad();
            long getProcessCpuLoad = (long) bean.getProcessCpuLoad();
            long getProcessCpuTime = (long) bean.getProcessCpuTime();
            
            System.out.println("getSystemCpuLoad: "+getSystemCpuLoad);
            System.out.println("getProcessCpuLoad: "+getProcessCpuLoad);
            System.out.println("getProcessCpuTime: "+getProcessCpuTime);
            
            hashMap.put("Actual total free system memory", mem.getActualFree()/1024/1024);
            hashMap.put("Actual total Used system memory", mem.getActualUsed()/1024/1024);
            hashMap.put("Total free system memory", mem.getFree()/1024/1024);
            hashMap.put("System Random Access Memory", mem.getRam());
            hashMap.put("Total system memory", mem.getTotal()/1024/1024);
            hashMap.put("Total used system memory", mem.getUsed()/1024/1024);
            hashMap.put("Memory used percent", mem.getUsedPercent());
            
            for (int i = 0; i < info.length; i++){
                CpuInfo cpuInfo = info[i];                
                cpuinfo.put("Total Core", Runtime.getRuntime().availableProcessors());
                cpuinfo.put("Total amount of CPU", cpuInfo.getMhz());
                cpuinfo.put("Cpu Model", cpuInfo.getModel());
                cpuinfo.put("CPU cache", cpuInfo.getCacheSize());

            }
            
            
            
            jSONObject.put("Cpu Info", cpuinfo);
            jSONObject.put("Memory Info", hashMap);
            
            
            
            OutputStream os = con.getOutputStream();
            os.write(jSONObject.toString().getBytes("UTF-8"));
            os.close();
            
            con.disconnect();
                
        } catch (JSONException ex) {
            Logger.getLogger(JavaApplication1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SigarException ex) {
            Logger.getLogger(JavaApplication1.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Actual total free system memory: "
                + mem.getActualFree() / 1024 / 1024+ " MB");
        System.out.println("Actual total used system memory: "
                + mem.getActualUsed() / 1024 / 1024 + " MB");
        System.out.println("Total free system memory ......: " + mem.getFree()
                / 1024 / 1024+ " MB");
        System.out.println("System Random Access Memory....: " + mem.getRam()
                + " MB");
        System.out.println("Total system memory............: " + mem.getTotal()
                / 1024 / 1024+ " MB");
        System.out.println("Total used system memory.......: " + mem.getUsed()
                / 1024 / 1024+ " MB");
        System.out.println("Total Memory Used Percent.......: " + mem.getUsedPercent());
        System.out.println("\n**************************************\n");

    }
    public static void main(String[] args) throws MalformedURLException, IOException {
        // TODO code application logic here
        getInformationsAboutMemory();
    }
    
}
